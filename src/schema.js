const { gql } = require('apollo-server');

const typeDefs = gql`
    type Product {
        id: String
        name: String
        price: Float
        image: String
    }

    type Customer {
        firstName: String
        lastName: String
        email: String
    }

    input CustomerRegistrationInput {
        login: String
        email: String
        last_name: String
    }

    input CustomerInput {
        password: String,
        customer: CustomerRegistrationInput
    }

    type Query {
        products(q: String): [Product]
    }

    type Mutation {
        registerCustomer(input: CustomerInput): Customer
    }
`;

module.exports = typeDefs;
