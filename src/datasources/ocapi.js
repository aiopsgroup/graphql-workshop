const { RESTDataSource } = require('apollo-datasource-rest');

class OcapiAPI extends RESTDataSource {

    siteId = 'RefArch';
    apiVersion = '21_9';
    clientId = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';

    authToken = null;
    
    constructor() {
        super();
        this.baseURL = 'https://zzuz-001.sandbox.us01.dx.commercecloud.salesforce.com/s/' + this.siteId + '/dw/shop/v' + this.apiVersion;
    }

    didReceiveResponse(response, _request) {
        // Is the response comming from a request for authorization tokens?
        if (_request.url.indexOf('customers/auth') > 0 && this.authToken === null) {
            const authorization = response.headers.get('Authorization');
            const [type, token] = authorization.split(' ');

            if (token) {
                this.authToken = token;
            }
        }

        return super.didReceiveResponse(response, _request);
    }

    willSendRequest(request) {
        request.params.set('client_id', this.clientId);

        if (this.authToken) {
            request.headers.set('Authorization', 'Bearer ' + this.authToken);
        }
    }

    async auth() {
        const response = await this.post('customers/auth', {
            type: "guest"
        });

        return response;
    }

    /*
    async prices(productId) {
        const response = await this.get('products/' + productId + '/prices');

        return response.price;
    }

    async images(productId) {
        const response = await this.get('products/' + productId + '/images');

        return response.image_groups[0].images[0].link;
    }

    async search(query) {
        const response = await this.get('product_search', {
            q: query
        });

        const result = Array.isArray(response.hits) ? response.hits.map(hit => this.hitReducer(hit)) : []; 

        return result;
    }

    async registerCustomer(customerData) {
        try {
            await this.auth();
        } catch(e) {
            console.log(e);
        }
        
        try {
            const response = await this.post('customers', new Object({...customerData}));

            // Auth token expiress if sucessfull
            if (response.hashed_login) {
                this.authToken = null;
            }

            return {
                firstName: response.first_name,
                lastName: response.last_name,
                email: response.email
            }
        } catch(e) {
            console.log(e.message);
        }

        return null;
    }

    hitReducer(hit) {
        return {
            id: hit.product_id,
            name: hit.product_name
        }
    }
    */
}

module.exports = OcapiAPI;
