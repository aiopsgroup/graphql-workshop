const { ApolloServer, gql } = require('apollo-server');

const typeDefs = require('./schema');

const OCAPI = require('./datasources/ocapi');
const resolvers = require('./resolvers');

const server = new ApolloServer({
    typeDefs,
    /*
    resolvers,
    dataSources: () => (
        {
            ocapi: new OCAPI
        }
    )
    */
});

server.listen().then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
});